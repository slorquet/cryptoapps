This repository contains application that demonstrate the use of the
NuttX Cryptographic Manager.

It is intended to help and debug the development of this new subsystem.

HOW TO USE
----------

Clone in your NuttX application directory
 * cd <$(APPDIR)>
 * git clone https://bitbucket.org/slorquet/cryptoapps crypto
 * cd include
 * ln -s ../crypto/export crypto

Configure to include apps and libcrypto
 * make menuconfig
 * Application Configuration
 * Cryptographic Tools
 * Select the crypto lib and ct tool

Then compile with NSH and run the ct tool.
