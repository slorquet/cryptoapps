/****************************************************************************
 * include/crypto/crypto.h
 *
 *   Copyright (C) 2018 Sebastien Lorquet. All rights reserved.
 *   Author:  Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef _APPS_INCLUDE_CRYPTO_CRYPTO_H_
#define _APPS_INCLUDE_CRYPTO_CRYPTO_H_

#include <stdint.h>
#include <stdbool.h>

int crypto_sessionopen(void);
int crypto_sessionclose(int session);

/* Module management */

int crypto_modinfo(int session, bool first, FAR char *name, uint32_t alg,
                   uint32_t *flags);

int crypto_modselect(int session, FAR char *name);

int crypto_modauth(int session, uint32_t step, uint32_t len, FAR uint8_t * data,
                   uint32_t rspbuflen, FAR uint8_t *response);

/* Key management */

int crypto_keyinfo(int session, bool first, FAR uint32_t *enumerator,
                   FAR uint32_t *len, FAR uint32_t *flags);

int crypto_keycreate(int session, uint32_t keyid, uint32_t flags,
                     uint32_t length, FAR const uint8_t *value);

int crypto_keydelete(int session, uint32_t id);

int crypto_keygen(int session, uint32_t keyid, uint32_t rngalg);

int crypto_keyupdate(int session, uint32_t keyid, uint32_t component,
                     uint32_t unwrapalg, uint32_t buflen, FAR uint8_t *buffer);

int crypto_keyread(int session, uint32_t keyid, uint32_t component,
                   uint32_t buflen, FAR uint8_t *buffer);

int crypto_keycopy(int session, uint32_t destid, uint32_t flags,
                   uint32_t srcid);

/* Algorithm management */

int crypto_alginfo(int session, uint32_t id, FAR uint32_t *blocklen);

int crypto_alginit(int session, uint32_t algid, uint32_t algop, uint32_t keyid);

int crypto_algsetup(int session, uint32_t setid, uint32_t length,
                    FAR uint8_t *value);

int crypto_algstatus(int session, uint32_t statid, uint32_t buflen,
                     FAR uint8_t *value);

int crypto_algupdate(int session, uint32_t len_in, FAR const uint8_t *data_in,
                     uint32_t len_outbuf, FAR uint8_t *data_out);

int crypto_algfinish(int session, uint32_t len_in, FAR const uint8_t *data_in,
                     uint32_t len_outbuf, FAR uint8_t *data_out);

#endif /* _APPS_INCLUDE_CRYPTO_CRYPTO_H_ */

