/****************************************************************************
 * crypto/ct/ct_modenum.c
 *
 *   Copyright (C) 2018 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <crypto/crypto.h>
#include <nuttx/crypto/manager.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: ct_modenum
 *
 * Description: Enumerate all crypto modules registered on the system.
 *              TODO later add module filtering options based on alg or other
 *              module features.
 ****************************************************************************/

int ct_modenum(int argc, char *argv[])
{
  int      session;
  int      ret;
  char     name[32];
  uint32_t flags;
  int      count;

  /* Open a session with the cryptographic manager */

  session = crypto_sessionopen();

  if(session < 0)
    {
      fprintf(stderr, "Failed to open a crypto session, errno=%d\n", errno);
    }

  /* Enumerate modules - ALG_NONE means all modules */

  /* Look at first module */

  count = 0;
  memset(name, 0, sizeof(name));
  ret = crypto_modinfo(session, true, name, ALG_NONE, &flags); 
  while(ret == OK)
    {
      printf("Module: %s, flags: 0x%08X\n", name, flags);
      count += 1;
      /* Look at next module */

      memset(name, 0, sizeof(name));
      ret = crypto_modinfo(session, false, name, ALG_NONE, &flags); 
    }
  printf("Module enumeration stopped with errno=%d, Total modules:%d\n",
         errno, count);
  /* Release the session */

  crypto_sessionclose(session);

  return 0;
}
