/****************************************************************************
 * crypto/ct/ct_testdes.c
 *
 *   Copyright (C) 2018 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include <crypto/crypto.h>
#include <nuttx/crypto/manager.h>

/****************************************************************************
 * Public Functions
 ****************************************************************************/

static int ct_dotestdes(const uint8_t *plain,
                        const uint8_t *key,
                        const uint8_t *cipher)
{
  int session;
  int keyid;
  uint8_t temp[8];
  int i;
  int ret;
  uint8_t test;

  /* Open a session with the cryptographic manager */

  session = crypto_sessionopen();

  if(session < 0)
    {
      fprintf(stderr, "Failed to open a crypto session, errno=%d\n", errno);
      return -1;
    }

  /* Create a key that will be deleted at the end of the session */
  keyid = crypto_keycreate(session, 0, KEYFLAG_TYPE_DES | KEYFLAG_VOLATILE,
                           8, key);

  if(keyid < 0)
    {
      fprintf(stderr, "Failed to create a temp key, errno=%d\n", errno);
      return -1;
    }

  printf("key created with ID %08X\n", keyid);

  ret = crypto_alginit(session, ALG_DES_ECB, ALGOP_CIPHER, keyid);

  if(ret<0)
    {
      fprintf(stderr, "Failed to init DES, errno=%d\n", errno);
      return -1;
    }

  ret = crypto_algfinish(session, 8, plain, 8, temp);

  if(ret<0)
    {  
      fprintf(stderr, "Failed to cipher with DES, errno=%d\n", errno);
      return -1;
    }

  /* Compare with known result */

  for(i=0; i<8; i++)
    {
      temp[i] ^= cipher[i];
    }

  test = 0;
  for(i=0; i<8; i++)
    {
      test |= temp[i];
    }

  if(test)
    {  
      fprintf(stderr, "DES cipher failed\n");
      return -1;
    }

  /* Release the session */

  crypto_sessionclose(session);
  
  return 0;
}

/****************************************************************************
 * Name: ct_testdes
 *
 * Description: Enumerate all crypto modules registered on the system.
 *              TODO later add module filtering options based on alg or other
 *              module features.
 ****************************************************************************/

int ct_testdes(void)
{

  ct_dotestdes((const uint8_t*)"\xDE\xAD\xBE\xEF\xCA\xFE\xFA\xDE", /* Plain */
               (const uint8_t*)"\x01\x23\x45\x67\x89\xAB\xCD\xEF", /* Key */
               (const uint8_t*)"\x4A\xB4\xD8\xAD\x47\xDE\xD2\x4B"  /* Cipher */
               );

  return 0;
}
